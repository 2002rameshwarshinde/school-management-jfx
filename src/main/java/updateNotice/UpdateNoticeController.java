package updateNotice;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import Common.ApiEndPoint;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class UpdateNoticeController {

	
	@FXML
	private DatePicker datePicker;

	@FXML
	private ComboBox combobox;

	@FXML
	private Button updatenotice;

	@FXML
	private TextField notice;
	
	@FXML
	private TextField idnumber;
	
	 private void updateData() {
	    	
	  
	    	String IDnumber = idnumber.getText();

			String apiUrl = ApiEndPoint.UPDATENOTICE + IDnumber;
	        
	        try {
	        	
	            URL url = new URL(apiUrl); 
	            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	            connection.setRequestMethod("PUT");
	            connection.setRequestProperty("Content-Type", "application/json");
	            connection.setDoOutput(true);
	           
	            String payload = "{"
	                    + "\"noticeDate\":\"" + datePicker.getValue() + "\","
	                    + "\"notice\":\"" + notice.getText() + "\","
	                    + "\"createdBy\":\"" + combobox.getValue() + "\""
	                    + "}";

	          
	            try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
	                outputStream.writeBytes(payload);
	                outputStream.flush();
	            }
	            int responseCode = connection.getResponseCode();
	            if (responseCode == HttpURLConnection.HTTP_OK) {
	               
	                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	                String response = reader.readLine();
	                reader.close();
	                System.out.println("Update Successful: " + response);
	            } else {
	              
	                System.out.println("Update Failed: " + responseCode);
	            }

	            connection.disconnect();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	 
	 public void updateNoticeData(ActionEvent event) {
		 updateData() ;
	 }
}
