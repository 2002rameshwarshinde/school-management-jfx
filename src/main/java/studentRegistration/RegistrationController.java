package studentRegistration;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class RegistrationController {

	@FXML
	private TextField mobNo;

	@FXML
	private TextField grNo;

	@FXML
	private Button next;


	public static TextField[] dataInfo = new TextField[0];

	public TextField getMobNo() {
		return mobNo;
	}




	public void setMobNo(TextField mobNo) {
		dataInfo[0] = mobNo;
		this.mobNo = mobNo;
	}




	public TextField getGrNo() {
		return grNo;
	}




	public void setGrNo(TextField grNo) {

		this.grNo = grNo;
	}




	public Button getNext() {
		return next;
	}




	public void setNext(Button next) {
		this.next = next;
	}




	public void next(ActionEvent event) {

		if (grNo.getText().equals(event))

		if (mobNo.getText().isEmpty() || grNo.getText().isEmpty()
				) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please Fill All The Fields.");
			alert.showAndWait();
			return;
		}

		new Registration2().show();
	}
}
