package Common;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtil {
	public static HttpURLConnection PostJsonRequest(String strUrl,String JsonRequest) throws IOException {
		URL ur = new URL(strUrl);
		HttpURLConnection Connection = (HttpURLConnection) ur.openConnection();
		Connection.setRequestMethod("POST");
		Connection.setRequestProperty("userId", "abcdef");

		Connection.setRequestProperty("Content-Type", "application/json");
		Connection.setDoOutput(true);

		OutputStream osObj = Connection.getOutputStream();
		osObj.write(JsonRequest.getBytes());

		osObj.flush();
		osObj.close();

		return Connection;
}

}