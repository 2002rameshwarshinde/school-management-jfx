package deleteNotice;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import Common.ApiEndPoint;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class DeleteNoticeController {

	@FXML
	private TextField idNumber;

	@FXML
	private Button delete;

	public void deleteNotice(ActionEvent event) {

		
			String iD = idNumber.getText();

			String apiUrl = ApiEndPoint.DELETENOTICE + iD;

			try {
				URL url = new URL(apiUrl);
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("DELETE");

				int responseCode = connection.getResponseCode();
				if (responseCode == HttpURLConnection.HTTP_OK) {
					System.out.println("Record deleted successfully");
				} else {
					System.out.println("Failed to delete record");
				}

				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String inputLine;
				StringBuilder response = new StringBuilder();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}

				in.close();
				connection.disconnect();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void showSuccessAlert(String message) {
			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setContentText(message);
			alert.showAndWait();
		}

		private void showErrorAlert(String message) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setContentText(message);
			alert.showAndWait();
		}
	

	}

