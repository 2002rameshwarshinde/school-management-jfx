package addFees;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import Common.ApiEndPoint;
import Common.HttpUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class AddFeesController {


	@FXML
	private TextField grnNo;

	@FXML
	private TextField studentName;

	@FXML
	private ComboBox appliedclass;

	@FXML
	private TextField totalFees;

	@FXML
	private TextField feesPaid;

	@FXML
	private TextField RemainingFees;

	@FXML
	private DatePicker date;

	@FXML
	private Button Add;

	public void addFees(ActionEvent event) throws IOException {

		if (  grnNo.getText().isEmpty()|| studentName.getText().isEmpty()||appliedclass.getValue()==null || totalFees.getText().isEmpty()|| feesPaid.getText().isEmpty()|| RemainingFees.getText().isEmpty()|| date.getValue()==null ) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill all The fields.");
			alert.showAndWait();
			return;
		}

		final String messageContent = "{\n" + "\"grnNo\"" + ":\"" + grnNo.getText() + "\", \r\n"
				+ "\"studentName\"" + ":\"" + studentName.getText() + "\", \r\n"
				+ "\"appliedClass\"" + ":\"" + appliedclass.getValue() + "\", \r\n"
				+ "\"totalFees\"" + ":\"" + totalFees.getText() + "\", \r\n"
				+ "\"feesPaid\"" + ":\"" + feesPaid.getText() + "\", \r\n"
				+ "\"remainingFees\"" + ":\"" + RemainingFees.getText() + "\", \r\n" + "\"acadmicYear\"" + ":\"" + date.getValue()
				+ "\" \r\n" + "\n}";

		System.out.println(messageContent);

		HttpURLConnection Connection = HttpUtil.PostJsonRequest(ApiEndPoint.FEESENTRY, messageContent);

		int responseCode = Connection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + responseCode);
		System.out.println("The POST Request Response Message : " + Connection.getResponseMessage());
		if (responseCode == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader inputst = new InputStreamReader(Connection.getInputStream());
			BufferedReader br = new BufferedReader(inputst);
			String input = null;
			StringBuffer stringBuffer = new StringBuffer();
			while ((input = br.readLine()) != null) {
				stringBuffer.append(input);
			}
			br.close();
			Connection.disconnect();

			System.out.println(stringBuffer.toString());

			String JsonResponse = Connection.getResponseMessage();

		} else {

			System.out.println("POST Request did not work.");

		}
	}

}


