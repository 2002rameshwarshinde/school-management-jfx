package timetableEntry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import Common.ApiEndPoint;
import Common.HttpUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class TimetableEntryController {
	@FXML
	private ComboBox weekday;

	@FXML
	private ComboBox TimeSlot;

	@FXML
	private TextField activity;

	@FXML
	private ComboBox appliedclass;

	@FXML
	private Button AddEntry;



	public void addEntry(ActionEvent event) throws IOException {

//	  if (  weekDay.getValue()==null)|| TimeSlot.getValue()==null||appliedclass.getValue()==null || activity.getText().isEmpty() ) {
//			Alert alert = new Alert(Alert.AlertType.ERROR);
//			alert.setTitle("Error");
//			alert.setHeaderText(null);
//			alert.setContentText("Please fill all The fields.");
//			alert.showAndWait();
//			return;
//		}

		final String messageContent = "{\n" +
			    "\"weekday\": \"" + weekday.getValue() + "\",\n" +
			    "\"timeSlot\": \"" + TimeSlot.getValue() + "\",\n" +
			    "\"activity\": \"" + activity.getText() + "\",\n" +
			    "\"className\": \"" + appliedclass.getValue() + "\"\n" +
			    "}";

		System.out.println(messageContent);

		HttpURLConnection Connection = HttpUtil.PostJsonRequest(ApiEndPoint.FEESENTRY, messageContent);

		int responseCode = Connection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + responseCode);
		System.out.println("The POST Request Response Message : " + Connection.getResponseMessage());
		if (responseCode == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader inputst = new InputStreamReader(Connection.getInputStream());
			BufferedReader br = new BufferedReader(inputst);
			String input = null;
			StringBuffer stringBuffer = new StringBuffer();
			while ((input = br.readLine()) != null) {
				stringBuffer.append(input);
			}
			br.close();
			Connection.disconnect();

			System.out.println(stringBuffer.toString());

			String JsonResponse = Connection.getResponseMessage();

		} else {

			System.out.println("POST Request did not work.");

		}
	}

}
