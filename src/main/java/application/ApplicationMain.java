package application;




import deleteLeave.DeleteLeave;
import deleteNotice.DeleteNotice;
import examSchedule.ExamSchedule;
import javafx.application.Application;
import javafx.stage.Stage;
import searchFees.SearchFees;
import searchLeave.SearchLeave;
import searchNotice.SearchNotice;
import stageMaster.StageMaster;
import timetableEntry.TimetableEntry;
import updateFees.UpdateFees;
import updateLeave.UpdateLeave;
import updateNotice.UpdateNotice;
import updateNotice.UpdateNoticeController;

public class ApplicationMain extends Application {

	public static void main(String args[]) {

		launch(args);

	}

	@Override
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
	 new  ExamSchedule().show();

	}
	
	
}
