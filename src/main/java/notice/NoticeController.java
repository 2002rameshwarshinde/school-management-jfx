package notice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import Common.ApiEndPoint;
import Common.HttpUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class NoticeController {

	@FXML
	private DatePicker datePicker;

	@FXML
	private ComboBox combobox;

	@FXML
	private Button create;

	@FXML
	private TextField notice;

	public void createNotice(ActionEvent event) throws IOException {

		if (datePicker.getValue()==null || combobox.getValue()==null || notice.getText().isEmpty()) {
			Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Please fill all The fields.");
			alert.showAndWait();
			return;
		}

		final String messageContent = "{\n" + "\"noticeDate\"" + ":\"" + datePicker.getValue() + "\", \r\n"
				+ "\"createdBy\"" + ":\"" + combobox.getValue() + "\", \r\n" + "\"notice\"" + ":\"" + notice.getText()
				+ "\" \r\n" + "\n}";

		System.out.println(messageContent);

		HttpURLConnection Connection = HttpUtil.PostJsonRequest(ApiEndPoint.NOTICEENTRY, messageContent);

		int responseCode = Connection.getResponseCode();
		System.out.println("Response from the server is: \n");
		System.out.println("The POST Request Response Code :  " + responseCode);
		System.out.println("The POST Request Response Message : " + Connection.getResponseMessage());
		if (responseCode == HttpURLConnection.HTTP_CREATED) {

			InputStreamReader inputst = new InputStreamReader(Connection.getInputStream());
			BufferedReader br = new BufferedReader(inputst);
			String input = null;
			StringBuffer stringBuffer = new StringBuffer();
			while ((input = br.readLine()) != null) {
				stringBuffer.append(input);
			}
			br.close();
			Connection.disconnect();

			System.out.println(stringBuffer.toString());

			String JsonResponse = Connection.getResponseMessage();

		} else {

			System.out.println("POST Request did not work.");

		}
	}

}
